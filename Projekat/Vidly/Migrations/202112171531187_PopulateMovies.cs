namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMovies : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) VALUES (  'Fast and furious 9', 1 , 12/6/2021 , 18/2/1996 , 5 , 5  )");
            Sql("INSERT INTO Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) VALUES (  'Free guy', 1 , 11/3/2020 , 18/12/1976 , 4 , 3  )");
            Sql("INSERT INTO Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) VALUES (  'Croods', 6 , 2/2/2021 , 3/5/1996 , 8 , 8  )");
            Sql("INSERT INTO Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) VALUES (  'Orfin', 3 , 30/6/2021 , 14/2/1996 , 10 , 10  )");
            Sql("INSERT INTO Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) VALUES (  'Tenet', 2 , 12/6/2021 , 18/2/1995 , 2 , 2  )");
            Sql("INSERT INTO Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) VALUES ( 'Soul', 6 , 1/6/2021 , 8/2/1997 , 2 , 2  )");
            Sql("INSERT INTO Movies ( Name, GenreId, DateAdded, ReleaseDate, NumberInStock, NumberAvailable) VALUES (  'SpiderMan', 6 , 12/6/2021 , 5/7/1998 , 3 , 3  )");
           
           



        }

        public override void Down()
        {
        }
    }
}
