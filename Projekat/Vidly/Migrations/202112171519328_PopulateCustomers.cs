namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateCustomers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Customers ( Name, IsSubscribedToNewsletter, MembershipTypeId, Birthdate) VALUES ( 'John', 0 , 1, 18/2/1996 )");
            Sql("INSERT INTO Customers ( Name, IsSubscribedToNewsletter, MembershipTypeId, Birthdate) VALUES ( 'Mery', 0 , 2, 15/8/1998 )");
            Sql("INSERT INTO Customers ( Name, IsSubscribedToNewsletter, MembershipTypeId, Birthdate) VALUES ( 'Nikol', 0 , 3, 24/9/1978 )");
        }
        
        public override void Down()
        {
        }
    }
}
